#!/bin/bash
#SBATCH --cpus-per-task=10
#SBATCH --mem=1G

module load bowtie2/2.4.4 samtools/1.15.1

reference_index="/shared/bank/arabidopsis_thaliana/TAIR10.1/bowtie2/Arabidopsis_thaliana.TAIR10.1_genomic"
file_id="KO1_1"

mkdir -p results

srun bowtie2 --threads=10 -x "${reference_index}" -U "data/${file_id}.fastq.gz" -S "results/${file_id}.sam"
srun samtools view -hbS -q 30 -o "results/${file_id}.filtered.bam" "results/${file_id}.sam"
srun samtools sort -o "results/${file_id}.bam" "results/${file_id}.filtered.bam"